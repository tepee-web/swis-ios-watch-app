//
//  AppDelegate.swift
//  Sleep When I Sleep
//
//  Created by David Frame on 23/11/2017.
//  Copyright © 2017 SWIS. All rights reserved.
//

import UIKit
import HealthKit
import UserNotifications
import WatchConnectivity
import AVFoundation
import MediaPlayer

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate {

    var window: UIWindow?
    var watchSessionReady: Bool = false
    let config = Config(pPlistName: "Config.plist")
    let healthStore = HKHealthStore()
    var sleepManager: SleepManager!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        initWatchSession()
        initHealthStore()
        initSleepManager()
        initNotifications()
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode(rawValue: convertFromAVAudioSessionMode(AVAudioSession.Mode.default)), options: [])
        return true
    }
    
    func initWatchSession() {
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    func initHealthStore() {
        let readableTypes = Set([HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
                                 HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
        healthStore.requestAuthorization(toShare: nil, read: readableTypes) { (success, error) in }
    }
    
    func initSleepManager() {
        sleepManager = SleepManager()
        sleepManager.restingHeartRate = config.get(name: "Resting Heart Rate") as! Double
        sleepManager.alertWhenAsleep = config.get(name: "Alert When Asleep") as! Bool
    }
    
    func initNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (success, error) in
            if success {
                self.sleepManager.canSendNotifications = true
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        config.set(name: "Resting Heart Rate", value: sleepManager.restingHeartRate)
        config.set(name: "Alert When Asleep", value: sleepManager.alertWhenAsleep)
        config.set(name: "Pause Audio When Asleep", value: sleepManager.pauseAudioWhenAsleep)
        config.save()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if (WCSession.default.isReachable) {
            watchSessionReady = true
        } else {
            let alert = UIAlertController(title: "Watch App Error", message: "Cannot communicate with the watch app, please relaunch this app once it is installed and running.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default){ Never -> Void in
                fatalError("Companion app not detected!")
            })
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        if let data = message["Sleeping"] {
            let sleeping = data as! Bool
            if (sleeping) {
                sleepManager.pauseAudio()
                sleepManager.triggerSleeping()
            }
        } else if message["Started"] != nil {
            sleepManager.triggerStart()
        }

    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
    
    func sendMessageToWatch(message: [String : Any]) {
        if (watchSessionReady) {
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionMode(_ input: AVAudioSession.Mode) -> String {
	return input.rawValue
}
