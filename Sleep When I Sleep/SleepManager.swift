//
//  SleepManager.swift
//  Sleep When I Sleep
//
//  Created by David Frame on 01/03/2018.
//  Copyright © 2018 SWIS. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import UserNotifications
import MediaPlayer

class SleepManager: NSObject, UNUserNotificationCenterDelegate {
    
    var canSendNotifications: Bool = false
    var restingHeartRate: Double = 0
    var alertWhenAsleep: Bool = false
    var pauseAudioWhenAsleep: Bool = true
    var audioManager: AudioManager!
    var showStopButton: Bool = false
    
    override init() {
        super.init()
        audioManager = AudioManager(parent: self)
        
    }
    
    func triggerSleeping() {
        if (alertWhenAsleep && canSendNotifications) {
            sendLocalNotification()
        }
    }
    
    func triggerStart() {
        showStopButton = false
        audioManager.stopInterruptingAudio()
    }
    
    func pauseAudio() {
        showStopButton = true
        audioManager.interruptAudio()
    }
    
    func sendLocalNotification() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings(completionHandler: {(settings) in
            if settings.authorizationStatus == .authorized {
                let content = UNMutableNotificationContent()
                content.title = "Have you fallen asleep?"
                content.body = "We've paused audio playback for now."
                content.sound = UNNotificationSound.default
                content.categoryIdentifier = "FallenAsleepCategory"
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
                let request = UNNotificationRequest(identifier: "FallenAsleep", content: content, trigger: trigger)
                UNUserNotificationCenter.current().delegate = self
                UNUserNotificationCenter.current().add(request) {(error) in
                    if (error != nil) {
                        debugPrint(error!.localizedDescription as String)
                    }
                }
            }
        })
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.identifier == "FallenAsleep" {
            completionHandler([.alert,.sound])
        }
    }
    
}
