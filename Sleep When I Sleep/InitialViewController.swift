//
//  InitialViewController.swift
//  Sleep When I Sleep
//
//  Created by David Frame on 23/11/2017.
//  Copyright © 2017 SWIS. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    @IBOutlet weak var restingHeartRateLabel: UILabel!
    @IBOutlet weak var restingHeartRateSlider: UISlider!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var updateStopButtonTimer: Timer!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initRestingHeartRateUI()
    }
    
    override func viewDidLayoutSubviews() {
        let mGradient = CAGradientLayer()
        mGradient.frame = self.view.bounds
        let startColor = UIColor(red: 57/255, green:117/255, blue: 183/255, alpha: 1).cgColor
        let endColor = UIColor(red: 77/255, green:160/255, blue: 214/255, alpha: 1).cgColor
        let colors = [startColor, endColor]
        mGradient.startPoint = CGPoint(x: 0, y: 0.5)
        mGradient.endPoint = CGPoint(x: 1, y: 0.5)
        mGradient.colors = colors
        self.view.layer.insertSublayer(mGradient, at: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initRestingHeartRateUI() {
        restingHeartRateSlider.setValue(Float(appDelegate.sleepManager.restingHeartRate), animated: true)
        updateRestingHeartRateLabel(value: Double(appDelegate.sleepManager.restingHeartRate))
        let message = ["Resting Heart Rate": appDelegate.sleepManager.restingHeartRate]
        appDelegate.sendMessageToWatch(message: message)
    }
    
    func updateRestingHeartRateLabel(value: Double) {
        restingHeartRateLabel.text = "Resting heart rate: " + String(value) + " BPM"
    }
    
    @IBAction func restingHeartRateSliderChanged(_ sender: Any) {
        let restingHeartRateValue = round(restingHeartRateSlider.value * 2) / 2;
        updateRestingHeartRateLabel(value: Double(restingHeartRateValue))
        appDelegate.sleepManager.restingHeartRate = Double(restingHeartRateValue)
    }

    @IBAction func restingHeartRateSliderSet(_ sender: Any) {
        let message = ["Resting Heart Rate": appDelegate.sleepManager.restingHeartRate]
        appDelegate.sendMessageToWatch(message: message)
    }

    @IBAction func tapStopButton(_ sender: UIButton) {
        appDelegate.sleepManager.triggerStart()
    }
    
}

