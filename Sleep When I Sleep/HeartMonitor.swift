//
//  HeartMonitor.swift
//  Sleep When I Sleep
//
//  Created by Darren Anderson on 05/04/2018.
//  Copyright © 2018 31interactive. All rights reserved.
//

import Foundation
import HealthKit

class HeartMonitor {
    
    var sleepManager: SleepManager!
    var observerQuery: HKObserverQuery!
    var appDelegate: AppDelegate!
    
    let healthStore = HKHealthStore()
    
    init(sleepManager: SleepManager, appDelegate: AppDelegate) {
        self.sleepManager = sleepManager
        
        guard HKHealthStore.isHealthDataAvailable() == true else {
            return
        }
        
        let readableTypes: Set<HKSampleType> = [HKQuantityType.quantityType(forIdentifier:
            HKQuantityTypeIdentifier.heartRate)!]
        healthStore.requestAuthorization(toShare: nil, read: readableTypes) { (success, error) in }
        
        initialiseQuery()
    }
    
    private func updateHeartRate() {
        debugPrint("Updating heart rate")
    }
    
    private func initialiseQuery() {
        guard let heartRateSampleType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return }
        observerQuery = HKObserverQuery(sampleType: heartRateSampleType, predicate: nil) {
            (query, completionHandler, error) in
            
            if error != nil {
                debugPrint("Error creating observer query.")
            }
            
            self.updateHeartRate()
        }
    }
    
    func start() {
        healthStore.execute(observerQuery)
    }
    
    func stop() {
        healthStore.stop(observerQuery)
    }
    
}
