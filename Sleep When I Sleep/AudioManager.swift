//
//  AudioManager.swift
//  Sleep When I Sleep
//
//  Created by Darren Anderson on 09/07/2018.
//  Copyright © 2018 SWIS. All rights reserved.
//

import Foundation
import AVKit
import MediaPlayer

class AudioManager: NSObject {
    var audioPlayer: AVAudioPlayer!
    var parent: SleepManager!
    
    init(parent: SleepManager) {
        super.init()
        
        self.parent = parent
        
        MPRemoteCommandCenter.shared().playCommand.isEnabled = false
        MPRemoteCommandCenter.shared().stopCommand.isEnabled = true
        MPRemoteCommandCenter.shared().stopCommand.addTarget(handler: {(_) -> MPRemoteCommandHandlerStatus in
            self.stopInterruptingAudio()
            return .success
        })
        MPRemoteCommandCenter.shared().pauseCommand.isEnabled = true
        MPRemoteCommandCenter.shared().pauseCommand.addTarget(handler: {(_) -> MPRemoteCommandHandlerStatus in
            self.parent.triggerStart()
            return .success
        })

        do {
            let path = Bundle.main.path(forResource: "ocean.wav", ofType:nil)!
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            audioPlayer.numberOfLoops = -1;
        } catch {
            debugPrint("Error creating audio player!")
        }
    }
    
    func interruptAudio() {
        let nowPlayingInfo: [String: Any] = [
            MPMediaItemPropertyTitle: "Crashing Waves",
            MPMediaItemPropertyArtist: "Sleep When I Sleep",
            MPMediaItemPropertyPlaybackDuration: audioPlayer.duration
        ]
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
            UIApplication.shared.beginReceivingRemoteControlEvents()
            audioPlayer.play()
        } catch {
            print("Error starting audio session.")
        }
    }
    
    func stopInterruptingAudio() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            UIApplication.shared.endReceivingRemoteControlEvents()
            audioPlayer.stop()
        } catch {
            print("Error stopping audio session.")
        }
    }
}
