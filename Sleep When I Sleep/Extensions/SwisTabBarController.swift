//
//  SwisTabBarControllerViewController.swift
//  Sleep When I Sleep
//
//  Created by Colin Masters on 2019-10-17.
//  Copyright © 2019 SWIS. All rights reserved.
//

import UIKit

class SwisTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = UIColor.red
        UITabBar.appearance().barTintColor = UIColor.black

        UITabBar.appearance().selectionIndicatorImage = UIImage().makeImageWithColorAndSize(color: UIColor.blue, size: CGSize(width: tabBar.frame.width/5, height: tabBar.frame.height))

        if let items = self.tabBar.items {
            for item in items {
                if let image = item.image {
                    item.image = image.withRenderingMode(.alwaysOriginal)
                }
            }
        }
    }
}
