//
//  PreferencesViewController.swift
//  Sleep When I Sleep
//
//  Created by David Frame on 23/11/2017.
//  Copyright © 2017 SWIS. All rights reserved.
//

import UIKit

class PreferencesViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var alertWhenAsleepSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initSettingsUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initSettingsUI() {
        alertWhenAsleepSwitch.setOn(appDelegate.sleepManager.alertWhenAsleep, animated: false)
    }
    
    func updateSettings() {
        appDelegate.sleepManager.alertWhenAsleep = alertWhenAsleepSwitch.isOn
    }

    @IBAction func alertWhenAsleepSwitchChanged(_ sender: Any) {
        updateSettings()
    }
    
    @IBAction func pauseAudioWhenAsleepSwitchChanged(_ sender: Any) {
        updateSettings()
    }
    
}
