//
//  Config.swift
//  Sleep When I Sleep
//
//  Created by David Frame on 27/03/2018.
//  Copyright Â© 2018 SWIS. All rights reserved.
//

import Foundation

class Config {
    
    var plistName: String
    
    var plistURL: URL
    
    var bundledPlistURL: URL
    
    var dictionary: [String:Any]!
    
    init(pPlistName: String) {
        plistName = pPlistName
        
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        plistURL = documentsUrl.first!.appendingPathComponent(plistName)
        
        let plistFileName = pPlistName.components(separatedBy: ".")[0]
        
        let path = Bundle.main.path(forResource: plistFileName, ofType: "plist")!
        bundledPlistURL = URL(fileURLWithPath: path)
        
        copyBundledPlistToDocuments()
        
        let data = try! Data(contentsOf: plistURL)
        dictionary = try! PropertyListSerialization.propertyList(from: data, format: nil) as! [String:Any]
    }
    
    private func copyBundledPlistToDocuments()
    {
        let fileManager = FileManager.default
        if (!fileManager.fileExists(atPath: plistURL.path)) {
            do {
                try FileManager.default.copyItem(atPath: bundledPlistURL.path, toPath: plistURL.path)
            } catch let error as NSError {
                debugPrint(error.description)
            }
        }
    }
    
    func get(name: String) -> Any? {
        return dictionary[name]
    }
    
    func set(name: String, value: Any) {
        dictionary.updateValue(value, forKey: name)
    }
    
    func save() {
        let plistData = try! PropertyListSerialization.data(fromPropertyList: dictionary, format: .xml, options: 0)
        try! plistData.write(to: plistURL)
    }
    
}
