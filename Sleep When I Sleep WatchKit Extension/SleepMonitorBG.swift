//
//  SleepMonitorBG.swift
//  Sleep When I Sleep WatchKit Extension
//
//  Created by Darren Anderson on 05/04/2018.
//  Copyright © 2018 SWIS. All rights reserved.
//

import WatchKit
import HealthKit
import Foundation
import UserNotifications

class SleepMonitorBG : NSObject, UNUserNotificationCenterDelegate {
    var interfaceController: InterfaceController!
    var extensionDelegate: ExtensionDelegate!
    var hkQuery: HKAnchoredObjectQuery!
    var workoutSession: HKWorkoutSession!
    var sessionActive: Bool!
    var endSession: Bool!
    var monitor: Bool!
    var sessionStopped: Bool!
    
    var startTime: Date!
    
    let healthStore = HKHealthStore()
    
    var heartBeat: HeartBeat?
    var bpmLabel: WKInterfaceLabel?
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    
    init(interfaceController: InterfaceController, extensionDelegate: ExtensionDelegate) {
        super.init()
        
        self.interfaceController = interfaceController
        self.extensionDelegate = extensionDelegate
        
        guard HKHealthStore.isHealthDataAvailable() == true else {
            return
        }
        
        // Request HealthKit authorisation.
        let readableTypes = Set([HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
        HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
        healthStore.requestAuthorization(toShare: nil, read: readableTypes) { (success, error) in }
        
        if(self.hkQuery == nil) {
            self.initialiseQuery()
        }
        
        if(self.workoutSession == nil) {
            self.initialiseWorkoutSession()
        }
        
        self.sessionActive = false
        self.endSession = false
        self.monitor = true
        self.sessionStopped = false
        
        let awakeCategory = UNNotificationCategory(identifier: "AwakeCategory",
                                                   actions: [],
                                                   intentIdentifiers: [],
                                                   options: .customDismissAction)
        
        UNUserNotificationCenter.current().setNotificationCategories([awakeCategory])
    }
    
    private func initialiseWorkoutSession() {
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = HKWorkoutActivityType.other
        workoutConfiguration.locationType = HKWorkoutSessionLocationType.indoor
        
        do {
            debugPrint("Creating workout session...")
            self.workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
        } catch let error as NSError {
            debugPrint(error)
            return
        }
    }
    
    func sendLocalNotification() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings(completionHandler: {(settings) in
            if settings.authorizationStatus == .authorized {
                debugPrint("Sending watch notification.")
                let content = UNMutableNotificationContent()
                content.title = "Are you awake?"
                content.body = "If you do not dismiss this notification within 10 seconds, your audio will be paused."
                content.sound = UNNotificationSound.default
                content.categoryIdentifier = "AwakeCategory"
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
                let notificationId = UUID().uuidString
                let request = UNNotificationRequest(identifier: notificationId, content: content, trigger: trigger)
                UNUserNotificationCenter.current().delegate = self
                UNUserNotificationCenter.current().add(request) {(error) in
                    if (error != nil) {
                        debugPrint(error!.localizedDescription as String)
                    }
                }
            } else {
                debugPrint("Can't send local watch notification. Unauthorized.")
            }
        })
    }
    
    private func initialiseQuery() {
        guard let heartRateSample = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return }
        self.hkQuery = HKAnchoredObjectQuery(type: heartRateSample, predicate: nil, anchor: anchor, limit: Int(HKObjectQueryNoLimit)) {
            (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            guard let newAnchor = newAnchor else { return }
            self.anchor = newAnchor
        }
        self.hkQuery.updateHandler = {(query, samples, deletedObjects, newAnchor, error) -> Void in
            self.anchor = newAnchor!
            debugPrint("Heart rate query update handler called")
            self.saveSleepAnalysis()
            self.isAsleep()
            guard let heartRateSamples = samples as? [HKQuantitySample] else { return }
            DispatchQueue.main.async() {
                if(self.sessionActive) {
                    guard let sample = heartRateSamples.first else { return }
                    var value = sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                    value = round(value * 2) / 2
                    self.heartBeat?.bpm = Double(value)
                    self.bpmLabel?.setText(String(value) + " BPM")
                    if (value <= self.extensionDelegate.sleepPoint && self.monitor) {
                        debugPrint("Threshold reached, sending message")
                        
                        self.endSession = true
                        self.sessionActive = false
                        self.sendLocalNotification()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(10)) {
                            if(self.endSession) {
                                self.interfaceController.dismiss()
                                let message = ["Sleeping": true]
                                self.extensionDelegate.sendMessageToPhone(message: message)
                                self.interfaceController.stopMonitoring()
                            }
                        }
                        
                    } else {
                        debugPrint("Threshold not reached.");
                    }
                }
            }
        }
    }
    
    func saveSleepAnalysis() {
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            let isAsleep = HKCategorySample(type:sleepType, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime, end: Date())
            healthStore.save(isAsleep) { (success, error) in
                debugPrint("\(success) \(error?.localizedDescription ?? "")")
            }
        }
    }
    
    func isAsleep() {
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 30, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                if error != nil { return }
                
                if let result = tmpResult {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            let isAsleep = (sample.value == HKCategoryValueSleepAnalysis.asleep.rawValue) ? true : false
                            if isAsleep {
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(10)) {
                                    if(self.endSession) {
                                        self.interfaceController.dismiss()
                                        let message = ["Sleeping": true]
                                        self.extensionDelegate.sendMessageToPhone(message: message)
                                        self.interfaceController.stopMonitoring()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            healthStore.execute(query)
        }
    
    }
    
    func setHeartBeat(heartBeat: HeartBeat) {
        self.heartBeat = heartBeat
    }
    
    func setBpmLabel(bpmLabel: WKInterfaceLabel) {
        self.bpmLabel = bpmLabel
    }
    
    func start() {
        startTime = Date()
        debugPrint("Starting workout session...")
        healthStore.start(self.workoutSession)
        
        debugPrint("Starting heart rate observer...")
        healthStore.execute(self.hkQuery)
        
        self.sessionActive = true
        self.monitor = true
        self.sessionStopped = false
        
        let message = ["Started": true]
        self.extensionDelegate.sendMessageToPhone(message: message)
    }
    
    func stop() {
        debugPrint("Ending workout session...")
        healthStore.end(self.workoutSession)
        healthStore.stop(self.hkQuery)
        self.initialiseQuery()
        self.initialiseWorkoutSession()
        
        self.sessionActive = false
        self.sessionStopped = true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if(!self.sessionActive && self.endSession && !self.sessionStopped) {
            self.endSession = false
            self.sessionActive = true
            self.extensionDelegate.sleepPoint = self.extensionDelegate.sleepPoint - 3.0
            self.monitor = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(20)) {
                self.monitor = true
            }
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
}
