//
//  ExtensionDelegate.swift
//  Sleep When I Sleep WatchKit Extension
//
//  Created by David Frame on 23/11/2017.
//  Copyright © 2017 SWIS. All rights reserved.
//

import WatchKit
import WatchConnectivity
import UserNotifications

protocol SleepPointMonitorDelegate: class {
    func didUpdateSleepPoint(with sleepPoint: Double)
}

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {
    
    var sleepPointDelegate: SleepPointMonitorDelegate!
    var watchSessionReady: Bool = false
    let config = Config(pPlistName: "WatchConfig.plist")
    var sleepPoint: Double!
    var initialSleepPoint: Double!
    var monitoring: Bool = false
    var canSendNotifications: Bool!
    
    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
        initWatchSession()
        initSleepPoint()
        initNotifications()
    }

    func initWatchSession() {
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    func initNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (success, error) in
            if success {
                self.canSendNotifications = true
            } else {
                self.canSendNotifications = false
                debugPrint("Error getting notification authorization")
            }
        }
    }
    
    func initSleepPoint() {
        sleepPoint = config.get(name: "Sleep Point") as? Double
        initialSleepPoint = sleepPoint
    }
    
    func saveSleepPoint() {
        config.set(name: "Sleep Point", value: sleepPoint + 3)
        config.save()
    }
    
    func applicationDidBecomeActive() {
        initSleepPoint()
    }

    func applicationWillResignActive() {
        saveSleepPoint()
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if (WCSession.default.isReachable) {
            watchSessionReady = true
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        if let data = message["Resting Heart Rate"], let delegate = self.sleepPointDelegate {
            let restingHeartRate = data as! Double
            var sleepingHeartRate = (restingHeartRate - (restingHeartRate * 0.08))
            sleepingHeartRate = (sleepingHeartRate * 2).rounded() / 2
            sleepPoint = sleepingHeartRate
            initialSleepPoint = sleepingHeartRate
            delegate.didUpdateSleepPoint(with: sleepPoint)
        }
    }
    
    func sendMessageToPhone(message: [String : Any]) {
        if (watchSessionReady) {
            saveSleepPoint()
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }

}
