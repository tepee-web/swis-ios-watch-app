//
//  HeartBeat.swift
//  Sleep When I Sleep WatchKit Extension
//
//  Created by David Frame on 06/03/2018.
//  Copyright © 2018 SWIS. All rights reserved.
//

import WatchKit
import Foundation

class HeartBeat {

    var iconLabel:WKInterfaceLabel
    
    var preBeatTimer:Timer!
    var postBeatTimer:Timer!
    
    var bpm:Double = 70
    
    var timerDuration: Double {
        return Double(60 / bpm)
    }
    
    init(iconLabel: WKInterfaceLabel) {
        self.iconLabel = iconLabel
    }
    
    init(iconLabel: WKInterfaceLabel, bpm: Double) {
        self.iconLabel = iconLabel
        self.bpm = bpm
    }
    
    func start() {
        setIconToDefaultState()
        startPreBeatTimer()
    }
    
    func stop() {
        if(preBeatTimer !== nil) {
            preBeatTimer.invalidate()
        }
        
        if(postBeatTimer !== nil) {
            postBeatTimer.invalidate()
        }
        
        setIconToDefaultState()
    }
    
    private func setIconToDefaultState()
    {
        iconLabel.setAttributedText(
            NSAttributedString(
                string: "❤️",
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20.0, weight: UIFont.Weight.bold)
                ]
            )
        )
    }
    
    private func setIconToBeatState()
    {
        iconLabel.setAttributedText(
            NSAttributedString(
                string: "❤️",
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 28.0, weight: UIFont.Weight.bold)
                ]
            )
        )
    }
    
    private func startPreBeatTimer() {
        setIconToDefaultState()
        preBeatTimer = Timer.scheduledTimer(
            timeInterval: timerDuration,
            target: self,
            selector: #selector(self.preBeatTimerDidEnd),
            userInfo: nil,
            repeats: false
        )
    }
    
    @objc private func preBeatTimerDidEnd(_ sender: Any) {
        startPostBeatTimer()
    }
    
    private func startPostBeatTimer() {
        setIconToBeatState()
        postBeatTimer = Timer.scheduledTimer(
            timeInterval: 0.2,
            target: self,
            selector: #selector(self.postBeatTimerDidEnd),
            userInfo: nil,
            repeats: false
        )
    }
    
    @objc private func postBeatTimerDidEnd(_ sender: Any) {
        startPreBeatTimer()
    }
    
}
