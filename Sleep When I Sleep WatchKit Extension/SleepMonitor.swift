//
//  SleepMonitor.swift
//  Sleep When I Sleep WatchKit Extension
//
//  Created by David Frame on 07/03/2018.
//  Copyright © 2018 SWIS. All rights reserved.
//

import WatchKit
import HealthKit
import Foundation

class SleepMonitor {
    
    var interfaceController: InterfaceController!
    var extensionDelegate: ExtensionDelegate!
    
    var startTime: Date!
    
    var heartBeat: HeartBeat?
    var bpmLabel: WKInterfaceLabel?
    var sessionActive = false
    let healthStore = HKHealthStore()
    var workoutSession: HKWorkoutSession!
    var messageSent = false
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    var heartQuery: HKQuery!
    
    init(interfaceController: InterfaceController, extensionDelegate: ExtensionDelegate) {
        self.interfaceController = interfaceController
        self.extensionDelegate = extensionDelegate
        guard HKHealthStore.isHealthDataAvailable() == true else {
            return
        }
        let readableTypes = Set([HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
                                 HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
        healthStore.requestAuthorization(toShare: nil, read: readableTypes) { (success, error) in }
    }
    
    func setHeartBeat(heartBeat: HeartBeat) {
        self.heartBeat = heartBeat
    }
    
    func setBpmLabel(bpmLabel: WKInterfaceLabel) {
        self.bpmLabel = bpmLabel
    }

    func heartRateStreamingQuery(workoutStartDate: NSDate) -> HKQuery? {
        saveSleepAnalysis()
        isAsleep()
        
        guard let heartRateSample = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return nil }
        let heartRateQuery = HKAnchoredObjectQuery(type: heartRateSample, predicate: nil, anchor: anchor, limit: Int(HKObjectQueryNoLimit)) {
            (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            guard let newAnchor = newAnchor else { return }
            self.anchor = newAnchor
        }
        heartRateQuery.updateHandler = {(query, samples, deletedObjects, newAnchor, error) -> Void in
            self.anchor = newAnchor!
            debugPrint("Heart rate query update handler called")
            guard let heartRateSamples = samples as? [HKQuantitySample] else { return }
            DispatchQueue.main.async() {
                debugPrint("Checking new heart rate sample")
                guard let sample = heartRateSamples.first else { return }
                var value = sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                value = round(value * 2) / 2;
                self.heartBeat?.bpm = Double(value)
                self.bpmLabel?.setText(String(value) + " BPM")
                if (value <= self.extensionDelegate.sleepPoint) {
                    if (!self.messageSent) {
                        debugPrint("Threshold reached, sending message")
                        let message = ["Sleeping": true]
                        self.extensionDelegate.sendMessageToPhone(message: message)
                        self.messageSent = true
                    }
                    self.interfaceController.stopMonitoring()
                } else {
                    debugPrint("Threshold not reached.");
                }
            }
        }
        return heartRateQuery
    }
    
    func saveSleepAnalysis() {
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            let isAsleep = HKCategorySample(type:sleepType, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime, end: Date())
            healthStore.save(isAsleep) { (success, error) in
                debugPrint("\(success) \(error?.localizedDescription ?? "")")
            }
        }
    }
    
    func isAsleep() {
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 30, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                if error != nil { return }
                
                if let result = tmpResult {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            let isAsleep = (sample.value == HKCategoryValueSleepAnalysis.asleep.rawValue) ? true : false
                            if isAsleep {
                                let message = ["Sleeping": true]
                                self.extensionDelegate.sendMessageToPhone(message: message)
                                self.messageSent = true
                            }
                        }
                    }
                }
            }
            healthStore.execute(query)
        }
    
    }
    
    func didSessionStart(date: NSDate) {
        debugPrint("Starting session")
        heartQuery = heartRateStreamingQuery(workoutStartDate: date)!
        healthStore.execute(heartQuery)
        
        // Tell the phone we've started.
        let message = ["Started": true]
        self.extensionDelegate.sendMessageToPhone(message: message)
    }
    
    func didSessionEnd(date: NSDate) {
        debugPrint("Stopping session")
        healthStore.stop(heartQuery)
    }
    
    func start() {
        startTime = Date()
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = HKWorkoutActivityType.other
        workoutConfiguration.locationType = HKWorkoutSessionLocationType.indoor
        do {
            workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
            healthStore.start(workoutSession)
        } catch let error as NSError {
            debugPrint(error)
        }
        sessionActive = true
        didSessionStart(date: NSDate())
    }
    
    func stop() {
        if(workoutSession !== nil) {
            healthStore.end(workoutSession)
        }
        sessionActive = false
        didSessionEnd(date: NSDate())
    }

}
