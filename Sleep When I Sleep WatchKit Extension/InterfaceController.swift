//
//  InterfaceController.swift
//  Sleep When I Sleep WatchKit Extension
//
//  Created by David Frame on 23/11/2017.
//  Copyright © 2017 SWIS. All rights reserved.
//

import UIKit
import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {
    
    let extensionDelegate = WKExtension.shared().delegate as! ExtensionDelegate
    
    var updateSleepPointLabelTimer: Timer?
    var heartBeat: HeartBeat!
    var sleepMonitor: SleepMonitorBG!
    
    var initialised: Bool = false
    
    @IBOutlet var sleepPointHeadingLabel: WKInterfaceLabel!
    @IBOutlet var sleepPointLabel: WKInterfaceLabel!
    @IBOutlet var heartIconLabel: WKInterfaceLabel!
    @IBOutlet var pressStartLabel: WKInterfaceLabel!
    @IBOutlet var currentLabel: WKInterfaceLabel!
    @IBOutlet var startButton: WKInterfaceButton!
    @IBOutlet var stopButton: WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        extensionDelegate.sleepPointDelegate = self
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        initSleepPointLabel()
        
        if(!self.initialised) {
            initHeartBeat()
            initSleepMonitor()
            self.initialised = true
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        if let timer = updateSleepPointLabelTimer {
            timer.invalidate()
        }
    }
    
    func initSleepPointLabel() {
        updateSleepPointLabel(extensionDelegate.sleepPoint)
    }
    
    func initHeartBeat() {
        heartBeat = HeartBeat(iconLabel: heartIconLabel)
    }
    
    func initSleepMonitor() {
        sleepMonitor = SleepMonitorBG(interfaceController: self, extensionDelegate: extensionDelegate)
        sleepMonitor.setHeartBeat(heartBeat: heartBeat)
        sleepMonitor.setBpmLabel(bpmLabel: currentLabel)
    }
    
    @objc func updateSleepPointLabel(_ sleepPoint: Double) {
        sleepPointLabel.setText(String(sleepPoint) + " BPM")
    }
    
    func startMonitoring() {
        startButton.setHidden(true)
        stopButton.setHidden(false)
        pressStartLabel.setHidden(true)
        currentLabel.setHidden(false)
        sleepMonitor.start()
        heartBeat.start()
        extensionDelegate.monitoring = true
    }
    
    func stopMonitoring() {
        heartBeat.stop()
        sleepMonitor.stop()
        currentLabel.setHidden(true)
        pressStartLabel.setHidden(false)
        stopButton.setHidden(true)
        startButton.setHidden(false)
        extensionDelegate.monitoring = false
        updateSleepPointLabel(extensionDelegate.sleepPoint)
    }
        
    @IBAction func startButtonPress() {
        startMonitoring()
    }
    
    @IBAction func stopButtonPress() {
        stopMonitoring()
    }
}

extension InterfaceController: SleepPointMonitorDelegate {
    func didUpdateSleepPoint(with sleepPoint: Double) {
        DispatchQueue.main.async {
            self.updateSleepPointLabel(sleepPoint)
        }
    }
}
